package com.dso.common

import androidx.compose.foundation.layout.*
import androidx.compose.material.Text
import androidx.compose.material.Button
import androidx.compose.material.Card
import androidx.compose.material.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun App() {

    var counter by remember { mutableStateOf(0) }

        Column (
            Modifier.fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center,
                ) {
            Text(
                counter.toString(),
                Modifier.padding(bottom = 10.dp),
                fontSize = 50.sp,
            )
            Button(
                onClick = {counter++}
            ){
                Text("Click me!", fontSize = 30.sp)
            }
        }
}
