# Compose multiplatform counter

#### Eng:

This project is a part of my Winter practice in СПбГУТ(SPbSUT).

this is a first part of me learning to work with Compose multiplatform.

here I made a simple counter app, that compiles to both desktop and mobile.

#### Ru:
Этот проект является частью моей зимней практики в СПбГУТ.

это начало моего изучения Compose multiplatform.

здесь я создал простой каунтер, который компилируется в дэсктопное и мобильное приложения.